<?php

namespace App;

class Ordenatrix
{
    const ASC = 'ASC';
    const DESC = 'DESC';
    private $list;
    private $original;
    private $log;
    /**
     * @var CliColors
     */
    private $colors;

    public function __construct(array $list = [], CliColors $cliColors)
    {
        $this->original = $list;
        $this->list = $list;
        $this->colors = $cliColors;
        $this->log = null;
    }

    /**
     * Métode per ordenar alfabeticament els elements d'un llistat
     *
     * @param bool $inverse
     * @return Ordenatrix
     */
    public function sort(bool $inverse = false): Ordenatrix
    {
        $this->log .= "sort ";
        $this->log .= $inverse ? "(inverse)" : "";
        $this->log .= "\n";

        $inverse ? rsort($this->list) : sort($this->list);

        return $this;
    }

    /**
     * Métode per ordenar segons tipus sigui l'element.
     * En aquest cas he considerat els tipus de Php i alfabèticament.
     *
     * @param bool $inverse
     * @return Ordenatrix
     */
    public function sortByType(bool $inverse = false, bool $log = true): Ordenatrix
    {
        if ($log) {
            $this->log .= "sortByType ";
            $this->log .= $inverse ? "(inverse)" : "";
            $this->log .= "\n";
        }
        $aux = [];
        foreach ($this->list as $item) {
            $aux[] = [gettype($item) => $item];
        }
        $inverse ? krsort($aux) : ksort($aux);
        $this->list = $aux;

        return $this;
    }

    /**
     * Métode per filtrar per tipus.
     * En aquest cas he considerat els tipus de Php i alfabèticament.
     *
     * @param string $type
     * @param bool $inverse
     * @return $this
     */
    public function filterByType(string $type, bool $inverse = false): Ordenatrix
    {
        $this->log .= "filterByType ({$type})";
        $this->log .= $inverse ? "(inverse)" : "";
        $this->log .= "\n";

        $this->list = array_filter($this->list, function ($item) use ($type) {
            return gettype($item) === $type;
        });

        $this->sortByType($inverse, false);

        return $this;
    }

    /**
     * Mètode per comprovar que un caràcter està comprès entre dues lletres de l'alfabet.
     *
     * @param $str string El caràcter, només es té en compte el primer caràcter en cas d'arribar més llarg.
     * @param $min string lletra minima per defecta 'a'
     * @param $max string lletra màxima per defecte 'z'
     * @return bool retorna un boleà
     */
    private function isBetween($str, $min = 'a', $max = 'z'): bool
    {
        $index = array_flip(range('a', 'z'));

        $min = $index[$min];
        $max = $index[$max];
        $str = $index[$str[0]];

        return (($str > $min) && ($str < $max));
    }

    /**
     * Métode per comprobar que té un número determinat de carácters, segons un determinat valor i rangs donats.
     *
     * @param $str string EL caràcter
     * @param int $length els caràcters que ha de contenir
     * @param string $startStr la lletra mínima
     * @param string $endStr la lletra màxima
     * @return bool retorna un boleà
     */
    private function nBetween($str, int $length, string $startStr, string $endStr): bool
    {
        /*if (!is_string($str) || mb_strlen($str) < $length) {
            return false;
        }*/

        $strArr = str_split($str);
        $countChars = array_filter($strArr, function ($item) use ($startStr, $endStr) {
            return $this->isBetween(strtolower($item), $startStr, $endStr);
        });

        return count($countChars) >= $length;
    }

    /**
     * Barrejar (Reordenar aleatòriament)
     */
    public function shuffle(): Ordenatrix
    {
        $this->log .= "shuffle \n";
        shuffle($this->list);

        return $this;
    }

    /**
     * Resetejar al contingut a l'array original
     */
    public function reset(): Ordenatrix
    {
        $this->log .= "reset \n";
        $this->list = $this->original;
        return $this;
    }

    /**
     * Eliminar tots els elements que no continguin més de 3 caràcters entre 'n' i 't' (minúscules o majúscules)
     */

    public function eliminarElemQueTinguinMenysDe3CaracterEntreNiT(): Ordenatrix
    {
        $this->list = array_filter($this->list, function ($item) {
            return $this->nBetween($item, 3, 'n', 't');
        });

        return $this;
    }

    /**
     * Recuperar el llistat
     */

    public function getList()
    {
        return $this->list;
    }

    public function __toString()
    {
        $message = $this->colors->getColoredString('With execution stack:', 'red');
        $contentText = $this->colors->getColoredString('array content is: ', 'blue');
        $curlist = print_r($this->list, true);
        return "{$message} \n{$this->log} \n{$contentText} \n{$curlist}";
    }

}