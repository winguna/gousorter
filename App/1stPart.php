<?php
include_once 'Ordenatrix.php';
include_once 'CliColors.php';

/**
 * Hola,
 * T'envio el plantejament de la prova. La prova consta de dues parts.
 * En la primera, l'execució és lliure, però planteja-ho en tots els aspectes com si fos per un entorn productiu.
 * Un cop tingui la teva proposta, et proporcionaré un segon enunciat i si ho creus convenient, podràs modificar la proposta.
 *
 * Hauria d'estar realitzat en PHP, ser senzill, autònom i que funcioni sense requerir cap framework.
 * Un cop tinguis la proposta, jo et passaré un codi amb unes crides d'exemple i les sortides que produeixen i podràs alterar la proposta si creus que cal. Si tens cap dubte, comentem-ho.
 *
 * DESCRIPCIÓ
 *
 * Et proporciono un array divers $input (més avall);
 * Cal una 'llibreria' que permeti realitzar amb els seus elements, en qualsevol ordre i tants cops com calgui, les següents modificacions:
 *
 * Ordenar (+inversament)
 * Ordenar per tipus (+inversament)
 * Filtrar per tipus (+inversament)
 * Barrejar (Reordenar aleatòriament)
 * Resetejar al contingut a l'array original
 * Eliminar tots els elements que no continguin més de 3 caràcters entre 'n' i 't' (minúscules o majúscules)
 *
 * Pel que fa a la sortida, a més de retornar el resultat final, el codi s'executarà via CLI i també s'hauria de poder imprimir el contingut de l'array en qualsevol moment, així com l'historial de les modificacions que s'ha realitzat per arribar-hi.
 *
 */

use App\CliColors;
use App\Ordenatrix;

$colors = new CliColors();

$input = ['Ontario', new stdClass, 'mike', 'papa', 'leptoN', ['arr1', 'arr2', 'arr3'], 1, 'interferon', 2, 'delta', new StdClass, '1', ['arr4', 'arr5', 'arr6'], '0', '2', 0, 1, 4];

$sorter = new Ordenatrix($input, $colors);

/*$sorter->printListWithMessage("Original Array");

$sorter->ordenarAsc();
$sorter->printListWithMessage("Array sorted ascending and alphabetically");

$sorter->ordenarDesc();
$sorter->printListWithMessage("Array sorted descending and alphabetically");

$sorter->ordenarPerTipusAsc();
$sorter->printListWithMessage("Array sorted by PHP Types ascending and alphabetically");
$sorter->ordenarPerTipusDesc();
$sorter->printListWithMessage("Array sorted by PHP Types descending and alphabetically");


$sorter->filtrarPerTipusAsc('string');
$sorter->printListWithMessage("Array filtered by PHP Type, this example: by string. The result is displayed in ascending order and alphabetically.");
$sorter->filtrarPerTipusDesc('string');
$sorter->printListWithMessage("Array filtered by PHP Type, this example: by string. The result is displayed in descending order and alphabetically.");

$sorter->barrejar();
$sorter->printListWithMessage("Randomly ordered");

$sorter->resetejar();
$sorter->printListWithMessage("We restore to the original values.");

$sorter->eliminarElemQueTinguinMenysDe3CaracterEntreNiT();
$sorter->printListWithMessage("Remove all elements that contain no more than 3 characters between 'n' and 't' (lowercase or uppercase)");*/


echo $sorter->reset()->shuffle()->filterByType('string', true)->sortByType(true);
echo $sorter->reset()->eliminarElemQueTinguinMenysDe3CaracterEntreNiT()->eliminarElemQueTinguinMenysDe3CaracterEntreNiT();
echo $sorter->reset()->filterByType('integer')->sort(true);