# Gousorter
## Playing with arrays

Requirements:

- Have docker installed https://www.docker.com/
- Clone this repository


## How to use

```sh
docker compose run --rm php bash
```

In this case we enter the container to perform our tests.

Once inside, you can run the php client.
In this case we will work with the file 1stPart:

```sh
php 1stPart.php
```

Review the results in the console output.

MIT

**Free Software, Hell Yeah!**
